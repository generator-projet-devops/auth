import {
  Controller,
  Patch,
  Delete,
  Body,
  HttpCode,
  Request,
  UseGuards,
  Post,
} from '@nestjs/common';
import { UserService } from './user.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { UpdateUserDto } from './dto/update-user.dto';
import { ValidateAccountDto } from './dto/validate-user.dto';
import { RegenerateValidateAccountDto } from './dto/regenerate-validate-user.dto';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}
  @Post('validate')
  @ApiOperation({ summary: 'Validate user account with a validation code' })
  @ApiResponse({ status: 200, description: 'Account validated successfully' })
  @ApiResponse({ status: 404, description: 'User not found or invalid code' })
  @HttpCode(200)
  async validateAccount(@Body() validateAccountDto: ValidateAccountDto) {
    return await this.userService.validateAccount(validateAccountDto);
  }
  @Post('regenerate-validation-code')
  @ApiOperation({ summary: 'Regenerate validation code and send it' })
  @ApiResponse({
    status: 200,
    description: 'Validation code regenerated and sent successfully',
  })
  @ApiResponse({ status: 404, description: 'User not found' })
  @HttpCode(200)
  async regenerateValidationCode(
    @Body() regenerateValidateAccountDto: RegenerateValidateAccountDto,
  ) {
    return this.userService.regenerateValidationCode(
      regenerateValidateAccountDto,
    );
  }

  @Patch()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Update a user' })
  @ApiResponse({ status: 200, description: 'User updated successfully' })
  @ApiResponse({ status: 404, description: 'User not found' })
  @UseGuards(JwtAuthGuard)
  updateUser(@Request() req, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.updateUser(req.user.id, updateUserDto);
  }

  @Delete()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Delete your personnal account' })
  @ApiResponse({ status: 204, description: 'User deleted successfully' })
  @ApiResponse({ status: 404, description: 'User not found' })
  @HttpCode(204)
  @UseGuards(JwtAuthGuard)
  deleteUser(@Request() req) {
    return this.userService.deleteOneById(req.user.sub.id);
  }
}
