import { IsString, IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class ValidateAccountDto {
  @IsString()
  @ApiProperty({
    example: '123456',
    description: 'The validation code of your user',
  })
  code: string;

  @IsEmail()
  @ApiProperty({
    example: 'johndoe@example45.com',
    description: 'The email of your user',
  })
  email: string;
}
