import { IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class RegenerateValidateAccountDto {
  @IsEmail()
  @ApiProperty({
    example: 'johndoe@example45.com',
    description: 'The email of your user',
  })
  email: string;
}
