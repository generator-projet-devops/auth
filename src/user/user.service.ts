import { ValidateAccountDto } from './dto/validate-user.dto';
import {
  Injectable,
  BadRequestException,
  InternalServerErrorException,
  NotFoundException,
  HttpStatus,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './schema/user.schema';
import { CreateUserDto } from './dto/create-user.dto';
import * as bcrypt from 'bcrypt';
import { MailService } from '../mail/mail.service';
import { RegenerateValidateAccountDto } from './dto/regenerate-validate-user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>,
    private mailService: MailService,
  ) {}

  async createUser(user: CreateUserDto): Promise<any> {
    // Check if the email is already in use
    const existingUser = await this.userModel.findOne({ email: user.email });
    if (existingUser) {
      throw new BadRequestException(
        'This email is already in use by another user.',
      );
    }
    // Check if the username is already in use
    const existingUsernameUser = await this.userModel
      .findOne({ username: user.username })
      .exec();
    if (existingUsernameUser) {
      throw new BadRequestException(
        'This username is already in use by another user.',
      );
    }

    // If the email is not already in use, continue with user creation
    try {
      const salt = bcrypt.genSaltSync(10);
      const hashedPassword = await bcrypt.hash(user.password, salt);
      const validationCode = this.generateValidationCode();
      const createdUser = new this.userModel({
        username: user.username,
        email: user.email,
        password: hashedPassword,
        validated: false,
        validationCode,
        timestamp: Date.now(),
      });

      this.mailService.sendMail(
        user.email,
        'Validation de votre compte',
        `Bonjour ${user.username},\n\nVotre code de validation est : ${createdUser.validationCode}`,
      );

      createdUser.save();
      return {
        statusCode: HttpStatus.CREATED,
        message: 'User created successfully',
      };
    } catch (error) {
      throw new InternalServerErrorException('Error creating the user.');
    }
  }

  async findOneById(id: string): Promise<User | null> {
    return this.userModel.findById(id);
  }

  async findOneWithEmail(email: string): Promise<User | null> {
    return await this.userModel.findOne({ email: email });
  }

  async updateUser(id: string, updateUserDto: any): Promise<User> {
    // Check if the user exists
    try {
      await this.userModel.findOne({ _id: id }).exec();
    } catch (error) {
      throw new NotFoundException('User not found');
    }
    // Update the user with the new information
    const updatedUser = await this.userModel
      .findByIdAndUpdate(id, updateUserDto, { new: true })
      .exec();

    return updatedUser;
  }

  async deleteOneById(userId: string): Promise<void> {
    const user = await this.userModel.findOne({ _id: userId }).exec();
    if (!user) {
      //
      throw new NotFoundException('Utilisateur non trouvé');
    }
    await this.userModel.findOneAndDelete({ _id: userId }).exec();
  }
  async validateAccount(validateAccountDto: ValidateAccountDto): Promise<any> {
    const user = await this.userModel
      .findOne({
        validationCode: validateAccountDto.code,
        email: validateAccountDto.email,
      })
      .exec();

    if (user) {
      if (user.validated) {
        throw new BadRequestException('This account is already validated');
      }

      this.mailService.sendMail(
        user.email,
        'Votre compte est validé',
        `Bonjour ${user.username},\n\nVotre compte est validé`,
      );
      return await this.userModel
        .findByIdAndUpdate(user._id, { validated: true }, { new: true })
        .exec();
    } else {
      throw new BadRequestException('User not found or invalid code');
    }
  }
  async regenerateValidationCode(
    regenerateValidateAccountDto: RegenerateValidateAccountDto,
  ): Promise<any> {
    const validationCode = this.generateValidationCode();
    const user = await this.userModel.findOne({
      email: regenerateValidateAccountDto.email,
    });
    // If the user is not found, throw an error
    if (!user) {
      throw new BadRequestException('This email is not used by any user');
    }
    // If the user is already validated, throw an error
    if (user.validated) {
      throw new BadRequestException('This account is already validated');
    }

    try {
      await this.userModel
        .findByIdAndUpdate(
          user._id,
          { validationCode: validationCode },
          { new: true },
        )
        .exec();
      this.mailService.sendMail(
        user.email,
        'Validation de votre compte',
        `Bonjour ${user.username},\n\nVotre code de validation est : ${validationCode}`,
      );
    } catch (error) {
      throw new BadRequestException(error);
    }
  }
  public generateValidationCode(): string {
    const min = 100000;
    const max = 999999;
    const randomCode = Math.floor(Math.random() * (max - min + 1)) + min;
    return randomCode.toString();
  }
}
