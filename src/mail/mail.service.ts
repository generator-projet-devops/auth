import { Injectable } from '@nestjs/common';
import * as nodemailer from 'nodemailer';

@Injectable()
export class MailService {
  private transporter;

  constructor() {
    this.transporter = nodemailer.createTransport('smtp://maildev:1025');
  }

  async sendMail(to: string, subject: string, text: string) {
    const info = await this.transporter.sendMail({
      from: 'my-generator@example.com',
      to,
      subject,
      text,
    });

    console.log('Message sent: %s', info.messageId);
  }
}
