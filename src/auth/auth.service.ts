import { HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { User } from '../user/schema/user.schema';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<User | null> {
    const user = await this.userService.findOneWithEmail(email);

    if (user && (await bcrypt.compare(password, user.password))) {
      if (!user.validated) {
        throw new UnauthorizedException('Account not Validated');
      }
      return user;
    }
    return null;
  }

  async login(user: User) {
    const payload = {
      sub: {
        id: user._id,
      },
    };
    return {
      statusCode: HttpStatus.OK,
      access_token: await this.jwtService.sign(payload, {
        privateKey: process.env.PRIVATE_KEY,
      }),
      refresh_token: await this.jwtService.sign(payload, {
        privateKey: process.env.PRIVATE_KEY,
        expiresIn: '120',
      }),
    };
  }
  async refreshToken(user: User) {
    const payload = {
      sub: {
        id: user._id,
      },
    };
    return {
      access_token: await this.jwtService.sign(payload, {
        privateKey: process.env.PRIVATE_KEY,
      }),
      refresh_token: await this.jwtService.sign(payload, {
        privateKey: process.env.PRIVATE_KEY,
        expiresIn: '1h',
      }),
    };
  }
}
